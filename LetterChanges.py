# Using the Python language, have the function LetterChanges(str) take the str parameter being passed and modify it using the following
# algorithm. Replace every letter in the stri # ng with the letter following it in the alphabet (ie. c becomes d, z becomes a).
# Then capitalize every vowel in this new string (a, e# , i, o, u) and finally return this modified string.
# Input:"hello*3"
# Output:"Ifmmp*3"
#
# Input:"fun times!"
# Output:"gvO Ujnft!"

str="fun times!"
def LetterChanges(a_string):
    letters = 'abcdefghijklmnopqrstuvwxyz'
    encoded_string = ''
    for letter in a_string.lower():
        if letter not in letters:
            encoded_string += letter
        elif letter == 'z':
            encoded_string += 'a'
        else:
          index = letters.index(letter)
          encoded_string += letters[index + 1]
    capitalized_encoded = ''

    for char in encoded_string:
        if char in 'aeiouy':
            capitalized_encoded += char.upper()
        else:
            capitalized_encoded += char
    return capitalized_encoded

print(LetterChanges("fun times!"))
print(LetterChanges("hello*3"))

